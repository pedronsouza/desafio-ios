//
//  ShotTableViewCell.m
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import "ShotTableViewCell.h"

@interface ShotTableViewCell()

@end

@implementation ShotTableViewCell
- (instancetype)initWithShot:(Shot *)shot {
    self = [super init];
    if (self) {
        self.shot = shot;
    }
    
    return self;
}
- (void)awakeFromNib {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setTitle:(NSString *)title {
    self.lblTitle.text = title;
}

- (void)setTotalViews:(NSString *)totalViews {
    self.lblViewsCount.text = totalViews;
}
@end
