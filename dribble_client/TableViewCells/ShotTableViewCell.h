//
//  ShotTableViewCell.h
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shot.h"
@interface ShotTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblViewsCount;
@property (weak, nonatomic) IBOutlet UIImageView *imgShot;
@property (nonatomic, copy) Shot *shot;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (instancetype) initWithShot:(Shot *)shot;
- (void) setTitle:(NSString *)title;
- (void) setTotalViews:(NSString *)totalViews;
@end
