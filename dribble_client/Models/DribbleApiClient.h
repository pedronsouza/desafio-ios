//
//  DribbleApiClient.h
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <AFNetworking/AFNetworking.h>
#import "PopularShotsObject.h"

@interface DribbleApiClient : NSObject
+ (instancetype) sharedInstance;
- (void) fetchShotsWithPage:(NSInteger) page andCompletionBlock:(void(^)(PopularShotsObject *results))completionBlock;
@end
