//
//  PopularShotsObject.m
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import "PopularShotsObject.h"

@implementation PopularShotsObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"page" : @"page",
             @"per_page" : @"per_page",
             @"totalPages" : @"pages",
             @"shots" : @"shots"};
}
+ (NSValueTransformer *)shotsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:Shot.class];
}
@end
