//
//  Shot.h
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
@interface Shot : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy, readonly) NSNumber *shotId;
@property (nonatomic, copy, readonly) NSNumber *totalViews;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *shotDescription;
@property (nonatomic, copy, readonly) NSString *image;
@end
