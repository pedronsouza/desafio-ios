//
//  DribbleApiClient.m
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import "DribbleApiClient.h"


#define kShotsEndpoint @"https://api.dribbble.com/shots/popular"

static DribbleApiClient *sharedInstance = nil;

@interface DribbleApiClient()
@property (nonatomic, strong) AFHTTPRequestOperationManager *httpManager;
@end

@implementation DribbleApiClient

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.httpManager = [[AFHTTPRequestOperationManager alloc] init];
    }
    
    return self;
}

+ (instancetype)sharedInstance {
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[super alloc] init];
        
        return sharedInstance;
    }
}

- (void)fetchShotsWithPage:(NSInteger)page andCompletionBlock:(void (^)(PopularShotsObject *))completionBlock {
    NSDictionary *paramerters = @{
                                  @"page" : @(page),
                                  @"per_page" : @10};
    
    [self.httpManager GET:kShotsEndpoint parameters:paramerters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"HTTP request done: %@", responseObject);
        NSError *error;
        
        PopularShotsObject *responseResult = [MTLJSONAdapter modelOfClass:[PopularShotsObject class] fromJSONDictionary:responseObject error:&error];
        
        if (error) {
            NSLog(@"Serialization Failed: %@", error);
        }
        
        if (completionBlock) {
            completionBlock(responseResult);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}

@end
