//
//  Shot.m
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import "Shot.h"

@implementation Shot
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"shotId" : @"id",
             @"totalViews" : @"views_count",
             @"shotDescription" : @"description",
             @"title" : @"title",
             @"image" : @"image_url" };
}
@end
