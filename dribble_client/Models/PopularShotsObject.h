//
//  PopularShotsObject.h
//  dribble_client
//
//  Created by Pedro Souza on 8/16/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "Shot.h"

@interface PopularShotsObject : MTLModel<MTLJSONSerializing>
@property (nonatomic, copy, readonly) NSString *page;
@property (nonatomic, copy, readonly) NSNumber *per_page;
@property (nonatomic, copy, readonly) NSNumber *totalPages;
@property (nonatomic, copy, readonly) NSArray *shots;
@end
