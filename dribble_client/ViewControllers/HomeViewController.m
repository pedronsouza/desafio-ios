//
//  ViewController.m
//  dribble_client
//
//  Created by Pedro Souza on 8/15/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import "HomeViewController.h"
#import "DribbleApiClient.h"
#import "Shot.h"
#import "ShotTableViewCell.h"
#import "ShotDetailsViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>

#define kShotDetailsSegueIdentifier @"shotDetailsSegue"

static NSString *shotTableCellIdentifier = @"ShotTableCell";
@interface HomeViewController ()
@property (assign, nonatomic) NSInteger page;
@property (nonatomic, copy) PopularShotsObject *paginatedShots;
@property (nonatomic, strong) NSMutableArray *shots;
@end

@implementation HomeViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.shots = [[NSMutableArray alloc] init];
    [self setupRefreshControl];
    [self loadShots];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) setupRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    
    [self.refreshControl addTarget:self action:@selector(refreshTableViewResults)
                  forControlEvents:UIControlEventValueChanged];
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:kShotDetailsSegueIdentifier sender:nil];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShotTableViewCell *cell = (ShotTableViewCell *) [tableView dequeueReusableCellWithIdentifier:shotTableCellIdentifier];
    Shot *currentShot = [self.shots objectAtIndex:indexPath.row];
    cell.imgShot.image = nil;
    
    [cell.activityIndicator startAnimating];
    [cell setTitle:currentShot.title];
    [cell.imgShot sd_setImageWithURL:[NSURL URLWithString:currentShot.image]];
    [cell setShot:currentShot];
    [cell setTotalViews:currentShot.totalViews.stringValue];
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.shots count];
}




- (void) loadMore {
    self.page++;
    
    if (self.page < self.paginatedShots.totalPages.integerValue) {
        [self loadShots];
    }
}


- (void) refreshTableViewResults {
    self.page = 1;
    __block HomeViewController *self2 = self;
    self2.shots = [[NSMutableArray alloc] init];
    [self loadShots];
}

- (void) loadShots {
    __block HomeViewController *self2 = self;
    [[DribbleApiClient sharedInstance] fetchShotsWithPage:self.page andCompletionBlock:^(PopularShotsObject *results) {
        self2.paginatedShots = results;
        [self2.shots addObjectsFromArray:results.shots];
        [self2.tableView reloadData];
        
        if (self.refreshControl.isRefreshing) {
            [self.refreshControl endRefreshing];
        }
        
    }];
}

#pragma mark TableView ScrollView delegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - (self.tableView.frame.size.height);
    
    if (actualPosition >= contentHeight) {
        [self loadMore];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShotDetailsSegueIdentifier]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Shot *shot = [self.shots objectAtIndex:indexPath.row];
        ShotDetailsViewController *detailsVC = segue.destinationViewController;
        detailsVC.shot = shot;
    }
}
@end
