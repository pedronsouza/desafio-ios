//
//  ShotDetailsViewController.h
//  dribble_client
//
//  Created by Pedro Souza on 8/17/15.
//  Copyright (c) 2015 Pedro Souza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Shot.h"

@interface ShotDetailsViewController : UIViewController
@property (nonatomic, copy) Shot *shot;
@end
